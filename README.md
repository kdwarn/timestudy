# timestudy

Frederick W. Taylor was a bastard, but it *is* helpful to keep track of the amount of time you spend on an activity if you're doing it for your own benefit. `timestudy` provides a foundation for apps (for instance, the [CLI](https://kdwarn.net/projects/timestudy/) that I've been developing) to help you in this endeavor. It is heavily inspired by [Timewarrior](https://timewarrior.net/). 

See [docs.rs/timestudy](https://docs.rs/timestudy) for crate documentation. See the [change log](https://codeberg.org/kdwarn/timestudy/src/branch/main/CHANGELOG.md) for the changes made with each release.

## Storage

Activities are stored in a file in the user's data directory, in a newly created subdirectory named "timestudy". See the [dirs crate](https://docs.rs/dirs/latest/dirs/) for the location of the data directory for various operating systems. (On Linux the file is located at `~/.local/share/timestudy/activities`.) A backup ("activities.bk") is also stored next to this file, along with a file for archiving tags ("archived_tags").

## Tests

Tests rely on the creation of a temporary file and writing and reading from it. It is placed in the temporary directory of the OS where the tests are run (e.g. /tmp on Linux). Unit tests use this by virtue of having the `#[test]` attribute, and integration tests use a feature to access this functionality.

**Clients built with this library should specify this feature (`test-utils`) in the dev-dependencies section of the Cargo.toml file and run any tests that would might alter stored activities as integration tests (that is, in files in the tests/ directory, interacting with the client as the end user would), so that they use a temporary file to store (and possibly change or delete) activities, rather than the real file for storing user activities. Otherwise, you may discover after running your tests that you've wiped your own activities that you diligently recorded.** Yes, I did this at one point. Here's the relevant bit for the Cargo.toml file:

```toml
[dependencies]
timestudy = "0.7" # or whatever the latest version is

[dev-dependencies]
timestudy = { version = "0.7", features = ["test-utils"] }
```

Note that there's also a module that contains some convenience functions for testing, `test_utils` (with an underscore, not a hyphen like the feature).

Finally, the tests must be run consecutively rather than in parallel, as the temporary data file is repeatedly getting created and removed during testing. This is configured in the .cargo/config.toml file, and clients should do the same.
