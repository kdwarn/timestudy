# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://codeberg.org/kdwarn/timestudy/compare/v0.10.4...HEAD)

## [0.10.4](https://codeberg.org/kdwarn/timestudy/compare/v0.10.3...v0.10.4) - 2024-01-06

### Added

* Function to generate some basic statistics about activities' tags.

## [0.10.3](https://codeberg.org/kdwarn/timestudy/compare/v0.10.0...v0.10.3) - 2023-10-21

### Fixed

* Newlines are removed from descriptions, preventing bug when reading file where description contains a newline.
* Trying to track an activity with exact same time as another activity is now prevented.
* Archived tags will be renamed or deleted appropriately.

## 0.10.1 & 0.10.2 - 2023-10-21

Immediately yanked, as they contained bugs introduced in the bugfixes. See v.10.3.

## [0.10.0](https://codeberg.org/kdwarn/timestudy/compare/v0.9.0...v0.10.0) - 2023-06-29

### Added

* A backup file is now created before making changes to the data file.

### Changed

* Renamed `ActivityError` to `TsError`.
* Several collections were changed from `Option<T>`s to `T`s. Where appropriate, `is_empty()` will need to be used instead of checking for `Some`/`None`:
  * the `tags` field in the `Activity` struct
  * `activities()` 
  * `past_activities()`

### Fixed

* Many small improvements to the code to make it more clear, concise, or performant.
* Deprecated `chrono` functions replaced.
* Add test for not-existing index

## [0.9.0](https://codeberg.org/kdwarn/timestudy/compare/v0.8.0...v0.9.0) - 2023-06-23

### Added

* Changelog (this document) was created.
* New file to store archived tags, ability to archive and unarchive tags.
* Ability to rename a tag across all activities.
* Ability to delete a tag from all activities.

## [0.8.0](https://codeberg.org/kdwarn/timestudy/releases/tag/v0.8.0)

Everything else.
