#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::*;

#[test]
fn stop_activity_errs_if_no_existing_activity() {
    test_utils::clean_up();
    assert!(matches!(
        Activity::stop(None),
        Err(TsError::NoExistingCurrentActivity)
    ))
}

#[test]
fn stop_activity_adds_end() {
    test_utils::clean_up();
    Activity::start(None, vec![], None).unwrap();
    assert!(Activity::get(0).unwrap().end.is_none());
    Activity::stop(None).unwrap();
    assert!(Activity::get(0).unwrap().end.is_some());
}

#[test]
fn stop_activity_writes_to_file_without_end_provided() {
    test_utils::clean_up();
    test_utils::create_activities(1);
    assert!(matches!(Activity::most_recent_past(), Ok(Some(_))))
}

#[test]
fn stop_activity_writes_to_file_with_end_provided() {
    test_utils::clean_up();
    let _ = Activity::start(None, vec![], None).unwrap();
    let dt = Utc::now();
    let _ = Activity::stop(Some(dt));
    let b = Activity::most_recent_past().unwrap();
    assert_eq!(dt, b.unwrap().end.unwrap())
}
