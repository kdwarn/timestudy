#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::{test_utils, *};

#[test]
fn most_recent_past_of_just_one_activity_works() {
    test_utils::clean_up();
    let start = Utc.with_ymd_and_hms(2022, 5, 24, 7, 30, 0).unwrap();
    let end = Utc.with_ymd_and_hms(2022, 5, 25, 7, 30, 0).unwrap();
    Activity::start(Some(start), vec![], None).unwrap();
    Activity::stop(Some(end)).unwrap();
    let last = Activity::most_recent_past().unwrap();
    assert_eq!(start, last.unwrap().start);
}

#[test]
fn most_recent_past_of_multiple_activities_works() {
    test_utils::clean_up();
    Activity::track(
        Utc.with_ymd_and_hms(2022, 5, 24, 7, 30, 0).unwrap(),
        Utc.with_ymd_and_hms(2022, 5, 24, 8, 30, 0).unwrap(),
        vec![],
        None,
    )
    .unwrap();
    let start2 = Utc.with_ymd_and_hms(2022, 5, 24, 9, 00, 0).unwrap();
    let end2 = Utc.with_ymd_and_hms(2022, 5, 24, 9, 30, 0).unwrap();
    Activity::start(Some(start2), vec![], None).unwrap();
    Activity::stop(Some(end2)).unwrap();
    let last = Activity::most_recent_past().unwrap();
    assert_eq!(start2, last.unwrap().start);
}

#[test]
fn most_recent_past_returns_none_if_no_past() {
    test_utils::clean_up();
    let _ = Activity::start(None, vec![], None);
    assert!(matches!(Activity::most_recent_past(), Ok(None)))
}
