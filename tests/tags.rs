#![cfg(feature = "test-utils")]

use timestudy::{test_utils, *};

#[test]
fn create_activity_with_tags_succeeds() {
    test_utils::clean_up();
    let tags = vec!["work".to_string(), "api".to_string()];
    Activity::start(None, tags, None).unwrap();
    assert_eq!(
        Activity::get(0).unwrap().tags,
        vec!["work".to_string(), "api".to_string()]
    )
}

#[test]
fn tag_with_hash_char_causes_err() {
    test_utils::clean_up();
    let tags0 = vec!["# work".to_string()];
    let tags1 = vec!["#work".to_string()];
    let tags2 = vec!["wo#rk".to_string()];
    let tags3 = vec!["work#".to_string()];
    let tags4 = vec!["work #".to_string()];
    assert!(matches!(
        Activity::start(None, tags0, None),
        Err(TsError::DisallowedTagCharacter)
    ));
    assert!(matches!(
        Activity::start(None, tags1, None),
        Err(TsError::DisallowedTagCharacter)
    ));
    assert!(matches!(
        Activity::start(None, tags2, None),
        Err(TsError::DisallowedTagCharacter)
    ));
    assert!(matches!(
        Activity::start(None, tags3, None),
        Err(TsError::DisallowedTagCharacter)
    ));
    assert!(matches!(
        Activity::start(None, tags4, None),
        Err(TsError::DisallowedTagCharacter)
    ))
}

#[test]
fn add_tag_to_most_recent_succeeds() {
    test_utils::create_activities(3);
    // add the tag "dust" to most recent activity
    let a = Activity::get(0).unwrap();
    a.add_tag("dust".to_string()).unwrap();
    let b = Activity::most_recent_past().unwrap().unwrap();
    assert!(b.tags.contains(&"dust".to_string()))
}

#[test]
fn add_tag_to_second_succeeds() {
    test_utils::create_activities(3);
    // add the tag "crust" to 2nd most recent activity
    let a = Activity::get(1).unwrap();
    a.add_tag("crust".to_string()).unwrap();

    // check that it's now included in file
    assert!(Activity::get(1)
        .unwrap()
        .tags
        .contains(&"crust".to_string()));

    // lets also check that we still have 3 activities

    assert_eq!(activities().unwrap().len(), 3)
}

#[test]
fn add_second_tag_succeeds() {
    test_utils::create_activities(1);
    let a = Activity::get(0).unwrap();
    a.add_tag("rust".to_string()).unwrap();
    let b = Activity::get(0).unwrap();
    b.add_tag("bust".to_string()).unwrap();

    let tags = Activity::get(0).unwrap().tags;
    assert!(tags.contains(&"rust".to_string()));
    assert!(tags.contains(&"bust".to_string()));
}

#[test]
fn add_duplicate_tag_errs() {
    test_utils::create_activities(3);
    let a = Activity::get(0).unwrap();
    a.add_tag("rust".to_string()).unwrap();
    let b = Activity::get(0).unwrap();
    assert!(matches!(
        b.add_tag("rust".to_string()),
        Err(TsError::AlreadyTagged)
    ))
}

#[test]
fn remove_tag_succeeds() {
    test_utils::create_activity_with_tag("house".to_string());
    // first check that it was added correctly
    let a = Activity::get(0).unwrap();
    assert!(a.clone().tags.contains(&"house".to_string()));
    // now remove and check
    a.remove_tag("house".to_string()).unwrap();
    let b = Activity::get(0).unwrap();
    assert!(b.tags.is_empty())
}

#[test]
fn remove_tag_errs_if_activity_has_no_tags() {
    test_utils::create_activities(3);
    let a = Activity::get(0).unwrap();
    assert!(matches!(
        a.remove_tag("house".to_string()),
        Err(TsError::NoMatchingTag)
    ))
}

#[test]
fn remove_tag_errs_if_no_such_tag() {
    test_utils::create_activity_with_tag("house".to_string());
    let a = Activity::get(0).unwrap();
    assert!(matches!(
        a.remove_tag("home".to_string()),
        Err(TsError::NoMatchingTag)
    ))
}
