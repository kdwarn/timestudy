#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::{test_utils, *};

#[test]
fn delete_success() {
    test_utils::timed_activities(); // has 4 activities
    Activity::delete(0).unwrap();
    // now there should be 3
    assert_eq!(3, activities().unwrap().len())
}

#[test]
fn deletes_correct_activity() {
    // this has 4 activities; if we delete the first (newest) activity (in the VecDeque), the
    // new first one should have the start date of 2022-07-19:04:30
    test_utils::timed_activities();
    Activity::delete(0).unwrap();
    let activities = activities().unwrap();
    assert_eq!(
        activities[0].start,
        Utc.with_ymd_and_hms(2022, 7, 19, 4, 30, 0).unwrap()
    )
}

#[test]
fn delete_errs_if_out_of_bounds() {
    test_utils::timed_activities(); // has 4 activities
    assert!(matches!(
        Activity::delete(4), // no 5th activity
        Err(TsError::ActivityDoesNotExist)
    ))
}

#[test]
fn delete_errs_if_no_activities() {
    test_utils::clean_up();
    assert!(matches!(
        Activity::delete(0),
        Err(TsError::ActivityDoesNotExist)
    ))
}
