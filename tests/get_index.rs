#![cfg(feature = "test-utils")]

use timestudy::{test_utils, *};

#[test]
fn get_index_succeeds() {
    test_utils::clean_up();
    test_utils::create_activities(3);
    let acts = activities().unwrap();
    assert_eq!(acts[0].get_index().unwrap(), 0);
    assert_eq!(acts[2].get_index().unwrap(), 2);
}
