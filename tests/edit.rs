#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::{test_utils, *};

#[test]
fn edit_success() {
    test_utils::timed_activities(); // has 4 activities

    let new_time = Utc.with_ymd_and_hms(2022, 7, 19, 1, 45, 0).unwrap();
    let description = Some("Some things were done.".to_string());

    // change start time of oldest Activity and verify
    let activity_to_edit = Activity::get(3).unwrap();
    activity_to_edit
        .edit(
            new_time,
            Some(Utc.with_ymd_and_hms(2022, 7, 19, 2, 30, 0).unwrap()),
            vec![],
            description.clone(),
        )
        .unwrap();

    let activities = activities().unwrap();

    // edited start time correct
    assert_eq!(Activity::get(3).unwrap().start, new_time);
    // new description is correct
    assert_eq!(Activity::get(3).unwrap().description, description);

    // still have same number of activities
    assert_eq!(activities.len(), 4)
}

#[test]
fn edit_removing_description_succeeds() {
    test_utils::clean_up();
    let description = "Some things were done".to_string();
    Activity::start(None, vec![], Some(description.clone())).unwrap();

    let activity = Activity::get(0).unwrap();
    assert_eq!(activity.description.clone().unwrap(), description);

    let (start, end, tags) = (
        activity.start.clone(),
        activity.end.clone(),
        activity.tags.clone(),
    );
    activity.edit(start, end, tags, None).unwrap();
    let activity_edited = Activity::get(0).unwrap();
    assert_eq!(activity_edited.description, None)
}
