#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::{test_utils, *};

#[test]
fn delete_tag_succeeds() {
    test_utils::clean_up();
    let tags1 = vec!["work".to_string(), "api".to_string()];
    let tags2 = vec![
        "pleasure".to_string(),
        "api".to_string(),
        "rust".to_string(),
    ];
    Activity::track(Utc::now(), Utc::now(), tags1, None).unwrap();
    Activity::track(Utc::now(), Utc::now(), tags2, None).unwrap();
    delete_tag("api".to_string()).unwrap();

    assert_eq!(
        Activity::get(0).unwrap().tags,
        vec!["pleasure".to_string(), "rust".to_string()]
    );
    assert_eq!(Activity::get(1).unwrap().tags, vec!["work".to_string()])
}

#[test]
fn delete_tag_errs_when_no_activites() {
    test_utils::clean_up();
    assert!(matches!(
        delete_tag("api".to_string()),
        Err(TsError::NoActivities)
    ))
}

#[test]
fn delete_tag_errs_when_no_matching_tag() {
    test_utils::clean_up();
    let tags1 = vec!["work".to_string(), "api".to_string()];
    Activity::track(Utc::now(), Utc::now(), tags1, None).unwrap();
    assert!(matches!(
        delete_tag("other_tag".to_string()),
        Err(TsError::NoMatchingTag)
    ))
}

#[test]
fn delete_tag_errs_if_disallowed_char_in_tag() {
    test_utils::clean_up();
    assert!(matches!(
        delete_tag("#api".to_string()),
        Err(TsError::DisallowedTagCharacter)
    ))
}
