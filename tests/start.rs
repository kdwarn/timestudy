#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::{test_utils, *};

#[test]
fn start_activity_errs_if_existing_activity() {
    test_utils::clean_up();
    Activity::start(None, vec![], None).unwrap();
    assert!(matches!(
        Activity::start(None, vec![], None),
        Err(TsError::AlreadyExistingCurrentActivity)
    ));
}

#[test]
fn start_activity_adds_start() {
    test_utils::clean_up();
    let now = Utc::now();
    Activity::start(Some(now), vec![], None).unwrap();
    assert_eq!(Activity::get(0).unwrap().start, now);
}

#[test]
fn start_activity_writes_to_file_without_start_provided() {
    test_utils::clean_up();
    Activity::start(None, vec![], None).unwrap();
    assert!(!matches!(Activity::current(), Ok(None)));
}

#[test]
fn start_activity_writes_to_file_with_start_provided() {
    test_utils::clean_up();
    let dt = Utc::now();
    Activity::start(Some(dt), vec![], None).unwrap();
    assert_eq!(dt, Activity::current().unwrap().unwrap().start)
}

#[test]
fn start_activity_with_description_succeeds() {
    test_utils::clean_up();
    let description = "Some things were done".to_string();
    Activity::start(None, vec![], Some(description.clone())).unwrap();
    assert_eq!(Activity::get(0).unwrap().description.unwrap(), description)
}
