#![cfg(feature = "test-utils")]

use timestudy::{test_utils, *};

#[test]
fn get_success() {
    test_utils::timed_activities(); // has 4 activities

    assert!(matches!(Activity::get(3), Ok(_)))
}

#[test]
fn get_errs_if_out_of_bounds() {
    test_utils::timed_activities(); // has 4 activities
    assert!(matches!(
        Activity::get(4), // no 5th activity
        Err(TsError::ActivityDoesNotExist)
    ))
}

#[test]
fn get_errs_if_no_activities() {
    test_utils::clean_up();
    assert!(matches!(
        Activity::get(0),
        Err(TsError::ActivityDoesNotExist)
    ))
}
