#![cfg(feature = "test-utils")]

use timestudy::{test_utils, *};

#[test]
fn current_activity_none_if_no_activities() {
    test_utils::clean_up();
    assert!(matches!(Activity::current(), Ok(None)))
}

#[test]
fn current_activity_none_if_no_incomplete_activities() {
    test_utils::clean_up();
    test_utils::create_activities(2);
    assert!(matches!(Activity::current(), Ok(None)))
}

#[test]
fn current_activity_exists() {
    test_utils::clean_up();
    Activity::start(None, vec![], None).unwrap();
    assert!(matches!(Activity::current(), Ok(Some(_))));
}

#[test]
fn current_activity_none_if_it_was_stopped() {
    test_utils::clean_up();
    Activity::start(None, vec![], None).unwrap();
    Activity::stop(None).unwrap();
    assert!(matches!(Activity::current(), Ok(None)));
}
