#![cfg(feature = "test-utils")]

use chrono::{prelude::*, Duration};

use timestudy::{test_utils, *};

#[test]
fn start_time_cannot_be_in_future() {
    test_utils::clean_up();
    let future = Utc::now() + Duration::seconds(30);
    assert!(matches!(
        Activity::start(Some(future), vec![], None),
        Err(TsError::TimeCannotBeInFuture)
    ));
}

#[test]
fn stop_time_cannot_be_in_future() {
    test_utils::clean_up();
    Activity::start(None, vec![], None).unwrap();
    let future = Utc::now() + Duration::seconds(30);
    assert!(matches!(
        Activity::stop(Some(future)),
        Err(TsError::TimeCannotBeInFuture)
    ));
}

#[test]
fn stop_time_cannot_precede_start() {
    test_utils::clean_up();
    let start = Utc.with_ymd_and_hms(2022, 7, 4, 7, 30, 0).unwrap();
    let end = Utc.with_ymd_and_hms(2022, 7, 3, 7, 30, 0).unwrap();
    Activity::start(Some(start), vec![], None).unwrap();
    assert!(matches!(
        Activity::stop(Some(end)),
        Err(TsError::StopTimeCannotPrecedeStartTime),
    ));
    assert!(matches!(
        Activity::track(start, end, vec![], None),
        Err(TsError::StopTimeCannotPrecedeStartTime)
    ))
}
