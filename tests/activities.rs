// testing activities() and past_activities()
#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::{test_utils, *};

#[test]
fn activities_counts_are_accurate() {
    test_utils::clean_up();
    assert!(activities().unwrap().is_empty());
    assert!(past_activities().unwrap().is_empty());

    test_utils::clean_up();
    test_utils::create_activities(1);
    assert_eq!(activities().unwrap().len(), 1);
    assert_eq!(past_activities().unwrap().len(), 1);

    test_utils::clean_up();
    test_utils::create_activities(1);
    Activity::start(Some(Utc::now()), vec![], None).unwrap();
    assert_eq!(activities().unwrap().len(), 2);
    assert_eq!(past_activities().unwrap().len(), 1);

    test_utils::clean_up();
    test_utils::create_activities(2);
    assert_eq!(activities().unwrap().len(), 2);
    assert_eq!(past_activities().unwrap().len(), 2);

    test_utils::clean_up();
    test_utils::create_activities(10);
    Activity::start(Some(Utc::now()), vec![], None).unwrap();
    assert_eq!(activities().unwrap().len(), 11);
    assert_eq!(past_activities().unwrap().len(), 10);
}

#[test]
fn activities_are_in_reverse_chron_order() {
    test_utils::clean_up();
    test_utils::create_activities(3);
    let activities = activities().unwrap();
    assert!(activities[0].start > activities[2].start)
}
