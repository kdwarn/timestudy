#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::*;

#[test]
fn track_activity_writes_to_file() {
    test_utils::clean_up();
    let start = Utc::now();
    let tags = vec!["work".to_string(), "api".to_string()];
    Activity::track(start, Utc::now(), tags, None).unwrap();
    assert_eq!(start, Activity::most_recent_past().unwrap().unwrap().start);
}

#[test]
fn track_will_insert_activity_in_proper_order() {
    // activities() returns activities in reverse-chron order, so account for that

    // creates 4 activities with specific times
    test_utils::timed_activities();

    // this should first be saved as 3rd line, but then bumped to 4th after next
    // so when that's reverse with activities(), that makese it 3rd newest [2]
    let start1 = Utc.with_ymd_and_hms(2022, 7, 19, 4, 10, 0).unwrap();
    Activity::track(
        start1,
        Utc.with_ymd_and_hms(2022, 7, 19, 4, 20, 0).unwrap(),
        vec![],
        None,
    )
    .unwrap();
    // this one should be new last (oldest) task
    let start2 = Utc.with_ymd_and_hms(2022, 7, 19, 1, 0, 0).unwrap();
    Activity::track(
        start2,
        Utc.with_ymd_and_hms(2022, 7, 19, 1, 10, 0).unwrap(),
        vec![],
        None,
    )
    .unwrap();
    let activities = activities().unwrap();
    assert_eq!(activities[2].start, start1);
    assert_eq!(activities[5].start, start2);
}

#[test]
fn track_succeeds_if_no_overlap() {
    test_utils::timed_activities();
    assert!(Activity::track(
        Utc.with_ymd_and_hms(2022, 7, 19, 4, 10, 0).unwrap(),
        Utc.with_ymd_and_hms(2022, 7, 19, 4, 20, 0).unwrap(),
        vec![],
        None
    )
    .is_ok())
}

#[test]
fn track_errs_if_overlap() {
    test_utils::timed_activities();
    assert!(matches!(
        Activity::track(
            Utc.with_ymd_and_hms(2022, 7, 19, 3, 55, 0).unwrap(),
            Utc.with_ymd_and_hms(2022, 7, 19, 4, 20, 0).unwrap(),
            vec![],
            None
        ),
        Err(TsError::ActivitiesCannotOverlap)
    ))
}

#[test]
fn track_errs_if_same_times() {
    test_utils::clean_up();
    Activity::track(
        Utc.with_ymd_and_hms(2022, 7, 19, 3, 55, 0).unwrap(),
        Utc.with_ymd_and_hms(2022, 7, 19, 4, 20, 0).unwrap(),
        vec![],
        None,
    )
    .unwrap();
    assert!(matches!(
        Activity::track(
            Utc.with_ymd_and_hms(2022, 7, 19, 3, 55, 0).unwrap(),
            Utc.with_ymd_and_hms(2022, 7, 19, 4, 20, 0).unwrap(),
            vec![],
            None
        ),
        Err(TsError::ActivitiesCannotOverlap)
    ))
}

#[test]
fn track_adds_tags_and_description_properly() {
    test_utils::clean_up();
    let tags = vec!["one".to_owned(), "two".to_owned()];
    let description = Some("This is a description.".to_owned());
    Activity::track(Utc::now(), Utc::now(), tags.clone(), description.clone()).unwrap();
    let activity = Activity::most_recent_past().unwrap().unwrap();
    assert_eq!(tags, activity.tags);
    assert_eq!(description, activity.description);
}
