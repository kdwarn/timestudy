#![cfg(feature = "test-utils")]

use chrono::prelude::*;
use timestudy::{test_utils, *};

#[test]
fn rename_tag_succeeds() {
    test_utils::clean_up();
    let tags1 = vec!["work".to_string(), "old".to_string()];
    let tags2 = vec![
        "pleasure".to_string(),
        "old".to_string(),
        "rust".to_string(),
    ];
    Activity::track(Utc::now(), Utc::now(), tags1, None).unwrap();
    Activity::track(Utc::now(), Utc::now(), tags2, None).unwrap();
    rename_tag("old".to_string(), "new".to_string()).unwrap();

    let mut a0 = Activity::get(0).unwrap().tags;
    let mut a1 = Activity::get(1).unwrap().tags;
    a0.sort();
    a1.sort();

    assert_eq!(
        a0,
        vec![
            "new".to_string(),
            "pleasure".to_string(),
            "rust".to_string(),
        ]
    );
    assert_eq!(a1, vec!["new".to_string(), "work".to_string()])
}

#[test]
fn rename_tag_deletes_tag_if_would_be_duplicate() {
    test_utils::clean_up();
    let tags1 = vec!["new".to_string(), "old".to_string()];
    let tags2 = vec!["pleasure".to_string(), "old".to_string()];
    Activity::track(Utc::now(), Utc::now(), tags1, None).unwrap();
    Activity::track(Utc::now(), Utc::now(), tags2, None).unwrap();

    rename_tag("old".to_string(), "new".to_string()).unwrap();

    let mut a0 = Activity::get(0).unwrap().tags;
    a0.sort();

    assert_eq!(a0, vec!["new".to_string(), "pleasure".to_string()]);
    assert_eq!(Activity::get(1).unwrap().tags, vec!["new".to_string()])
}

#[test]
fn rename_tag_errs_when_no_activites() {
    test_utils::clean_up();
    assert!(matches!(
        rename_tag("old".to_string(), "new".to_string()),
        Err(TsError::NoActivities)
    ))
}

#[test]
fn rename_tag_errs_when_no_matching_tag() {
    test_utils::clean_up();
    let tags1 = vec!["work".to_string(), "api".to_string()];
    Activity::track(Utc::now(), Utc::now(), tags1, None).unwrap();
    assert!(matches!(
        rename_tag("old".to_string(), "new".to_string()),
        Err(TsError::NoMatchingTag)
    ))
}

#[test]
fn rename_tag_errs_if_disallowed_char_in_tag() {
    test_utils::clean_up();
    assert!(matches!(
        rename_tag("old".to_string(), "#new".to_string()),
        Err(TsError::DisallowedTagCharacter)
    ))
}
