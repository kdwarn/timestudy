#![cfg(feature = "test-utils")]

use chrono::prelude::*;

use timestudy::{test_utils, *};

#[test]
fn archive_unarchive_tag_succeeds() {
    test_utils::clean_up();
    let tags = vec!["work".to_string(), "api".to_string()];
    Activity::start(None, tags, None).unwrap();

    archive_tag("api".to_string()).unwrap();
    assert!(archived_tags().contains(&"api".to_string()));

    unarchive_tag("api".to_string()).unwrap();
    assert!(archived_tags().is_empty());
}

#[test]
fn archive_unarchive_multiple_tags_succeeds() {
    test_utils::clean_up();
    let tags = vec!["work".to_string(), "api".to_string()];
    Activity::start(None, tags.clone(), None).unwrap();

    archive_tag("api".to_string()).unwrap();
    archive_tag("work".to_string()).unwrap();
    assert!(tags.iter().all(|tag| archived_tags().contains(tag)));

    unarchive_tag("api".to_string()).unwrap();
    unarchive_tag("work".to_string()).unwrap();
    assert!(tags.iter().all(|tag| !archived_tags().contains(tag)));
}

#[test]
fn archive_unarchive_non_existing_tag_errs() {
    test_utils::clean_up();
    let tags = vec!["work".to_string(), "api".to_string()];
    Activity::start(None, tags, None).unwrap();

    assert!(matches!(
        archive_tag("other".to_string()),
        Err(TsError::NoMatchingTag)
    ));

    assert!(matches!(
        unarchive_tag("other".to_string()),
        Err(TsError::NoMatchingTag)
    ));
}

#[test]
fn archive_unarchive_no_activities_errs() {
    test_utils::clean_up();
    assert!(matches!(
        archive_tag("work".to_string()),
        Err(TsError::NoMatchingTag)
    ));
    assert!(matches!(
        unarchive_tag("work".to_string()),
        Err(TsError::NoMatchingTag)
    ))
}

#[test]
fn archive_unarchive_already_done_errs() {
    test_utils::clean_up();
    let tags = vec!["work".to_string(), "api".to_string()];
    Activity::start(None, tags, None).unwrap();
    Activity::stop(None).unwrap();

    archive_tag("work".to_string()).unwrap();
    assert!(matches!(
        archive_tag("work".to_string()),
        Err(TsError::TagAlreadyArchived)
    ));

    unarchive_tag("work".to_string()).unwrap();
    assert!(matches!(
        unarchive_tag("work".to_string()),
        Err(TsError::TagNotArchived)
    ));
}

#[test]
fn renaming_tag_renames_archived_tag() {
    test_utils::clean_up();
    Activity::start(None, vec!["this".to_string(), "that".to_string()], None).unwrap();
    Activity::stop(None).unwrap();
    archive_tag("this".to_string()).unwrap();
    rename_tag("this".to_string(), "that".to_string()).unwrap();
    dbg!(archived_tags());
    assert!(!archived_tags().contains(&"this".to_string()));
    assert!(archived_tags().contains(&"that".to_string()))
}

#[test]
fn deleting_tag_deletes_archived_tag() {
    test_utils::clean_up();
    Activity::start(None, vec!["work".to_string(), "api".to_string()], None).unwrap();
    Activity::stop(None).unwrap();
    archive_tag("work".to_string()).unwrap();
    delete_tag("work".to_string()).unwrap();
    assert!(archived_tags().is_empty());
}

#[test]
fn delete_activity_with_last_instance_of_tag_deletes_archived_tag() {
    test_utils::clean_up();
    Activity::track(
        Utc::now(),
        Utc::now(),
        vec!["work".to_string(), "api".to_string()],
        None,
    )
    .unwrap();
    Activity::track(
        Utc::now(),
        Utc::now(),
        vec!["api".to_string(), "this".to_string(), "that".to_string()],
        None,
    )
    .unwrap();
    archive_tag("work".to_string()).unwrap();
    Activity::delete(1).unwrap();
    assert!(!tags().unwrap().contains(&"work".to_string()));
    assert!(!archived_tags().contains(&"work".to_string()));
}
